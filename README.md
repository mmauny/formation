# Déploiement continu PHP
- [Initialisation du projet Symphony](#initialisation-du-projet-symphony)
- [Définition de l'image docker](#définition-de-limage-docker)
-  [Mise en place des tests unitaires](#mise-en-place-des-tests-unitaires)
- [Notre premier pipeline !](#notre-premier-pipeline-)
- [Ajout des tests dans le pipeline](#ajout-des-tests-dans-le-pipeline)
- [Ajout d'un controller](#ajout-dun-controller)
- [Liens](#liens)


### Initialisation du projet Symphony
- Simplement avec *Composer*
```sh 
composer create-project symfony/skeleton formation
```

#### Définition de l'image docker
On utilise une image php en recopiant l'ensemble de nos sources
Fichier Dockerfile :
```dockerfile
FROM php:7.3-alpine

COPY . /app

CMD php -S 0.0.0.0:80 app/public/index.php
```

On peut ensuite construire notre image
```shell
docker build . -t formation
```

Et lancer un *conteneur* basé sur cette image
```shell
docker run -p 80:80 -d formation
```

Un service basé sur cette image peut nous simplifier le développement en local :
```yml
version: '3'
services:
  app:
    image: maunym/formation
    ports:
    - 80:80
    volumes:
    - .:/app/
```

```shell
docker-composer run app
```


### Mise en place des tests unitaires
- Rajout de la dépendance vers phpunit
```sh 
composer require --dev symfony/phpunit-bridge
```

- Création d'une classe utilitaire
```php
<?php
// src/Util/Calculator.php
namespace App\Util;

class Calculator
{
    public function add($a, $b)
    {
        return $a + $b;
    }
}
```

- et de sa classe de test
```php
<?php

namespace App\Tests\Util;

use App\Util\Calculator;
use PHPUnit\Framework\TestCase;

class CalculatorTest extends TestCase
{
    public function testAdd()
    {
        $calculator = new Calculator();
        $result = $calculator->add(30, 12);

// assert that your calculator added the numbers correctly!
        $this->assertEquals(42, $result);
    }
}
```
- On peut ensuite exécuter nos tests
```sh 
php bin/phpunit tests
```

- Résultat attendu :
```
sh OK (1 test, 1 assertion)
```

#### Notre premier pipeline !
- Création d'un fichier *Jenkinsfile*
```
pipeline {
    agent { docker { image 'php' } }
    stages {
        stage('build') {
            steps {
                sh 'php --version'
            }
        }
    }
}
```
Pour l'instant notre stage build affiche juste la version de php

### Ajout des tests dans le pipeline
Exécution des tests via phpunit
```
    stage('test') {
        steps {
            sh 'php bin/phpunit tests'
        }
    }
```

Analyse SonarQube
```
    stage('Sonarqube') {
        environment {
            scannerHome = tool 'SonarQubeScanner'
        }
        steps {
            withSonarQubeEnv('sonarqube') {
                sh "${scannerHome}/bin/sonar-scanner -Dsonar.branch.name=${BRANCH_NAME}"
            }
        }
    }
```

#### Définition de l'image docker
On utilise une image php en recopiant l'ensemble de nos sources
Fichier Dockerfile :
```dockerfile
FROM php:7.3-alpine

COPY . /app

CMD php -S 0.0.0.0:80 app/public/index.php
```

On peut ensuite construire notre image
```shell
docker build . -t formation
```

ECort lancer un *conteneur* basé sur cette image
```shell
docker run -p 80:80 -d formation
```

### Ajout d'un controller
Pour verifier que nos changements sont pris en compte
src/Controller/BaseController.php
```php
<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends AbstractController
{
    /**
     * @Route("/", methods={"GET","HEAD"}, name="accueil")
     */
    public function getAccueil()
    {
        return new Response('Coucou');
    }
}
```
#### Liens
[WatchTower](https://github.com/containrrr/watchtower)
