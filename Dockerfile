FROM php:7.4-alpine

COPY . /app

CMD php -S 0.0.0.0:80 app/public/index.php
