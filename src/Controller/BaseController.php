<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BaseController extends AbstractController
{
    /**
     * @Route("/", methods={"GET","HEAD"}, name="accueil")
     */
    public function indexAction()
    {
        return new Response('<html><body style="background-color: #CACCFF"><br><br><h1 align="center">Coucou</h1></body></html>');
    }
}
